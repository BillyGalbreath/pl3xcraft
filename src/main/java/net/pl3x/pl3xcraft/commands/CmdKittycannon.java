package net.pl3x.pl3xcraft.commands;

import java.util.Random;

import net.pl3x.pl3xcraft.Pl3xcraft;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;

public class CmdKittycannon implements CommandExecutor {
	private Pl3xcraft plugin;
	private static Random random = new Random();
	
	public CmdKittycannon(Pl3xcraft plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("kittycannon")) {
			if (!cs.hasPermission("pl3xcraft.kittycannon")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(plugin.colorize("&4This command is only available to players!"));
				return true;
			}
			EntityType cat = EntityType.OCELOT;
			Player p = (Player) cs;
			final Ocelot ocelot = (Ocelot)p.getWorld().spawn(p.getEyeLocation(), cat.getEntityClass());
			if (ocelot == null)
				return true;
			ocelot.setCatType(Ocelot.Type.values()[random.nextInt(Ocelot.Type.values().length)]);
			ocelot.setTamed(true);
			ocelot.setVelocity(p.getEyeLocation().getDirection().multiply(2));
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				@Override
				public void run() {
					Location loc = ocelot.getLocation();
					ocelot.remove();
					loc.getWorld().createExplosion(loc, 0F);
				}
			}, 20);
			return true;
		}
		return false;
	}
}
