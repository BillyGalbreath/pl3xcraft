package net.pl3x.pl3xcraft.commands;

import net.pl3x.pl3xcraft.Pl3xcraft;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class CmdTop implements CommandExecutor {
	private Pl3xcraft plugin;
	
	public CmdTop(Pl3xcraft plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("top")) {
			if (!cs.hasPermission("pl3xcraft.top")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(plugin.colorize("&4This command is only available to players!"));
				return true;
			}
			Player p = (Player) cs;
			int topX = p.getLocation().getBlockX();
			int topZ = p.getLocation().getBlockZ();
			int topY = p.getWorld().getHighestBlockYAt(topX, topZ);
			p.teleport(new Location(p.getWorld(), p.getLocation().getX(), topY + 1, p.getLocation().getZ(), p.getLocation().getYaw(), p.getLocation().getPitch()), TeleportCause.COMMAND);
			return true;
		}
		return false;
	}
}
