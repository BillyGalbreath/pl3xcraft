package net.pl3x.pl3xcraft.commands;

import net.pl3x.pl3xcraft.Pl3xcraft;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Cow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class CmdFart implements CommandExecutor {
	private Pl3xcraft plugin;
	
	public CmdFart(Pl3xcraft plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("fart")) {
			if (!cs.hasPermission("pl3xcraft.fart")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(plugin.colorize("&4This command is only available to players!"));
				return true;
			}
			Player p = (Player) cs;
			p.setSneaking(true);
			Location loc = p.getLocation();
			loc.getWorld().createExplosion(loc, 0F);
			Cow cow = (Cow)p.getWorld().spawn(loc, EntityType.COW.getEntityClass());
			if (cow == null)
				return true;
			cow.setVelocity(p.getEyeLocation().getDirection().multiply(-2));
			return true;
		}
		return false;
	}
}
