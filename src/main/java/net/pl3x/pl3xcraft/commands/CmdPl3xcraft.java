package net.pl3x.pl3xcraft.commands;

import net.pl3x.pl3xcraft.Pl3xcraft;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdPl3xcraft implements CommandExecutor {
	private Pl3xcraft plugin;
	
	public CmdPl3xcraft(final Pl3xcraft plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pl3xcraft")) {
			if (!cs.hasPermission("pl3xcraft.pl3xcraft")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
				return true;
			}
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reload")) {
					plugin.reloadConfig();
					plugin.loadConfig();
					cs.sendMessage(plugin.colorize("&dPl3xcraft configuration reloaded."));
					return true;
				}
			}
			cs.sendMessage(plugin.colorize("&dPl3xcraft &7v" + plugin.getDescription().getVersion() + "&d."));
			return true;
		}
		return false;
	}
}
