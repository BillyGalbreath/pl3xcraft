package net.pl3x.pl3xcraft.commands;

import net.pl3x.pl3xcraft.Pl3xcraft;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSound implements CommandExecutor {
	private Pl3xcraft plugin;
	
	public CmdSound(Pl3xcraft plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("sound")) {
			if (!cs.hasPermission("pl3xcraft.sound")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for this command."));
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(plugin.colorize("&4This command is only available to players!"));
				return true;
			}
			if (args.length < 1) {
				cs.sendMessage(plugin.colorize("&4You MUST specify a Bukkit Sound!"));
				cs.sendMessage(cmd.getDescription());
				return true;
			}
			Float volume = 3F;
			Float pitch = 1F;
			if (args.length > 1) {
				try {
					volume = Float.valueOf(args[1]);
				} catch(Exception e) {
					cs.sendMessage(plugin.colorize("&4Volume MUST be a number!"));
					return true;
				}
			}
			if (args.length > 2) {
				try {
					pitch = Float.valueOf(args[2]);
				} catch(Exception e) {
					cs.sendMessage(plugin.colorize("&4Pitch MUST be a number!"));
					return true;
				}
			}
			String play = args[0].toUpperCase();
			Sound sound = null;
			try {
				sound = Sound.valueOf(play);
			} catch(Exception e) {
				cs.sendMessage(plugin.colorize("&4The sound &7" + play + "&4 is not a valid Bukkit Sound!"));
				return true;
			}
			Player p = (Player)cs;
			p.playSound(p.getLocation(), sound, volume, pitch);
			cs.sendMessage(plugin.colorize("&dPlaying sound."));
			return true;
		}
		return false;
	}
}
