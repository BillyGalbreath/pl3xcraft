package net.pl3x.pl3xcraft.listeners;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import net.pl3x.pl3xcraft.Pl3xcraft;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

public class PlayerListener implements Listener {
	private Pl3xcraft plugin;
	
	public PlayerListener(Pl3xcraft plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler()
	public void onGameModeChange(PlayerGameModeChangeEvent e) {
		Player p = e.getPlayer();
		if (p == null) return;
		String gamemode = e.getNewGameMode().toString();
		int x = (int) p.getLocation().getX();
		int y = (int) p.getLocation().getY();
		int z = (int) p.getLocation().getZ();
		String message = "&7" + p.getName() + "&3's gamemode has changed to " + gamemode;
		message = message + "&3(&9[&4" + p.getWorld().getName() + "&9]&3: &7" + x + "&3, &7" + y + "&3, &7" + z + "&3)";
		message = plugin.colorize("&9[&ePl3xCraft&9] &a*&b*&c*&dA&eL&aE&bR&cT&d*&e*&a* " + message);
		plugin.getServer().getConsoleSender().sendMessage(message);
		for (Player t : plugin.getServer().getOnlinePlayers()) {
			if (t.isOp() || t.hasPermission("pl3xcraft.see.gamemode")) {
				t.sendMessage(message);
			}
		}
		File log = new File(plugin.getDataFolder() + File.separator + "gamemodechange.log");
		try {
			if (!log.exists()) {
				log.getParentFile().mkdirs();
				log.createNewFile();
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(log, true));
			SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
			out.write(dateFormatGmt.format(new Date()) + " " +message);
			out.newLine();
			out.close();
		} catch (IOException ioe) {
			plugin.log("Failed to save to chat log file " + log.getAbsolutePath() + " : " + ioe.getMessage());
		}
	}
}
