package net.pl3x.pl3xcraft;

import java.io.IOException;
import java.util.logging.Level;

import net.pl3x.pl3xcraft.commands.*;
import net.pl3x.pl3xcraft.listeners.*;

import org.bukkit.Bukkit;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Pl3xcraft extends JavaPlugin {
	public void onEnable() {
		loadConfig();
		
		PluginManager pm = Bukkit.getPluginManager();
		
		pm.registerEvents(new PlayerListener(this), this);
		
		this.getCommand("fart").setExecutor(new CmdFart(this));
		this.getCommand("kittycannon").setExecutor(new CmdKittycannon(this));
		this.getCommand("pl3xcraft").setExecutor(new CmdPl3xcraft(this));
		this.getCommand("sound").setExecutor(new CmdSound(this));
		this.getCommand("top").setExecutor(new CmdTop(this));
		
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) { log("&4Failed to start Metrics: &e" + e.getMessage()); }
		
		log("Pl3xcraft v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log("Pl3xcraft Disabled.");
	}
	
	public void loadConfig() {
		if (!getConfig().contains("debug-mode")) getConfig().addDefault("debug-mode", "false");
		if (!getConfig().contains("color-logs")) getConfig().addDefault("color-logs", "true");
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public void log (final Object obj) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" +  getName() + "&3]&r " + obj));
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
	
	public String colorize(final String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
}
